import matplotlib.pyplot as plt
import sys

'''

"Documentation"
alias "WARNINGS!"

 
This Program is by no means intended to work for any usecase but mine (Visualising Data with GPS coordiates on a scatterplot).
If you still intend using it be assured that i have wasted no time debugging anything or making the code less ugly or even well documented...

use as follows (with data.csv and stations.csv in the same folder):
    ~> python3 map.py y/N data.csv stations.csv output.pdf/.png 
    
If the output is desired in pdf or png is up to you.
The y/N is just a swich for swapping the size and color determining columns

Where data.csv should be in the lineformat 
    station name , value determining the size of the blobs, value determining the color of the blobs
The latter two actually get normalized, so as long as they can be interpreted as a float its fine.

And where stations.csv should have the lineformat
    station name, ...(optional stuff)..., Longitude, Latitude
The middle Stuff just gets thrown away.  
'''

# variables
blob_size = 800
color_scaler = 254
figsizex = 50
figsizey = 60

# Get CSV-File with the data, the Format should be:
# uuid_station :: string, color-value :: float-lookalike, size-value :: float-lookalike

file = open(sys.argv[2], 'r')
lines = file.readlines()
file.close()

# now we want the words in format [[uuid_station, color_val, size_val],[...],...]
words = []
for line in lines[1:]:
    words.append(list(map(lambda s: s.replace('\n', ''), line.split(','))))

# now we calculate the neccesary min and max of the last two arguments,
# and obtain the needed normalized versions of colors and sizes
uuids = [x[0] for x in words]
if sys.argv[1] == "y":
    colors = [float(x[1]) for x in words]
    sizes = [float(x[2]) for x in words]
else:
    colors = [float(x[2]) for x in words]
    sizes = [float(x[1]) for x in words]

colormin = min(colors)
colormax = max(colors)
sizemin = min(sizes)
sizemax = max(sizes)
colors_scaled = list(map(lambda x: (x - colormin) / (colormax - colormin), colors))
sizes_scaled = list(map(lambda x: (x - sizemin) / (sizemax - sizemin), sizes))

# we now change the uuids into their respecting coordinates.
# we start by reading in the stations file and throwing away the excess data

file = open(sys.argv[3], 'r')
lines = file.readlines()
file.close()

# now we want the words in format [[uuid_station, ..., latitude, longitude],[...],...]
words = []
for line in lines[1:]:
    words.append(list(map(lambda s: s.replace('\n', ''), line.split(','))))

# now we throw away the excess middle and make it a dictionary
Stations = {x[0]: [float(x[-2]), float(x[-1])] for x in words}

# now we just make some final list to then plot
xs = []
ys = []
sizes_final = []
colors_final = []
#for q-percent quantils
colsort = sorted(colors_scaled)
collen = len(colors_scaled)
sizesort = sorted(sizes_scaled)
sizelen = len(sizes_scaled)

for i in range(len(uuids)):
    uuid = uuids[i]
    if uuid in Stations.keys():
        if Stations[uuid][0] != 0 and Stations[uuid][1] != 0:
            xs.append(Stations[uuid][1])
            ys.append(Stations[uuid][0])
            # here we use the percent quantiles, since otherwise the values are too close together.
            sizes_final.append(blob_size*sizesort.index(sizes_scaled[i])/sizelen)
            colors_final.append(color_scaler*(colsort.index(colors_scaled[i]))/collen)
        else:
            print("random zero case omitted, i dont know why those occur and i wont fix them")
    else:
        print("the uuid " + uuid + " has not been found in the dict")

# old version, doesnt work anymore.
# f = plt.figure(figsize=(50, 60))
# plt.axis([min(xs), max(xs), min(ys), max(ys)], 'off', 'scaled', 'tight')
# plt.scatter(xs, ys, s=sizes_final, c=colors_final, cmap='cool', alpha=0.3)

# here's the actual plot
# alpha is the transparency, cmap the colormap (see https://matplotlib.org/examples/color/colormaps_reference.html )
fig, axs = plt.subplots(1, 1, figsize=(figsizex, figsizey), constrained_layout=True)
psm = axs.scatter(xs, ys, s=sizes_final, c=colors_final, cmap='cool', alpha=0.3)
fig.colorbar(psm, ax=axs)

# give output
try:
    fig.savefig(sys.argv[4])
except:
    print("not saved figure, just showing")
    plt.show()
