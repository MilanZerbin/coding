import matplotlib.pyplot as plt
import math as m


# Use euclidean or other norm
def norm(x, y):
    return m.sqrt(x ** 2 + y ** 2)


class vec:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __add__(self, other):
        return vec(self.x + other.x, self.y + other.y)

    def getx(self):
        return self.x

    def gety(self):
        return self.y

    def __sub__(self, other):
        return vec(self.x - other.x, self.y - other.y)

    def __mul__(self, l):
        return vec(l * self.x, l * self.y)

    def __eq__(self, other):
        if self.x == other.x:
            if self.y == other.y:
                return True
        return False

    def abs(self):
        return norm(self.x, self.y)

    def normalize(self):
        if self == vec(0, 0):
            return vec(0, 0)
        return self * (1 / self.abs())

    def __repr__(self):
        return "(" + str(self.x) + ", " + str(self.y) + ")"

    def rotate(self, theta):
        return vec(m.cos(theta) * self.x - m.sin(theta) * self.y, m.sin(theta) * self.x + m.cos(theta) * self.y)


def iterate(start, startdir, stepsize, finaltime):
    two = start + startdir
    L = [start, two.normalize() * stepsize]
    step = L[-1] - L[-2]
    for i in range(finaltime):
        step = step.rotate(stepsize * i)
        L.append(L[-1] + step)
    return L


def splitveclist(L):
    xs, ys = [], []
    for v in L:
        xs.append(v.getx())
        ys.append(v.gety())
    return [xs, ys]


startpoint = vec(0, 0)
startdirection = vec(1, 0)
timestep = 1


for i in range(0, 4):
    plt.subplot(2, 2, i + 1)
    dt = timestep * (10 ** (-i))
    coordslist = iterate(startpoint, startdirection, dt, int(4 / dt))
    split = splitveclist(coordslist)
    xcoords = split[0]
    ycoords = split[1]
    plt.plot(xcoords, ycoords, linewidth=(4-i)*0.15)
    plt.axis('square')
    plt.xticks([])
    plt.yticks([])


#plt.show()
plt.savefig("eulercurve.png", dpi=2500)