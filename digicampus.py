from selenium import webdriver
import time

print("Please give automation Username:")
username = input()
print("Please give automation Password:")
password = input()

options = webdriver.FirefoxOptions()
options.add_argument('--ignore-certificate-errors')
options.add_argument("--test-type")
options.binary_location = "/usr/bin/firefox"
driver = webdriver.Firefox(firefox_options=options)
driver.get('https://digicampus.uni-augsburg.de/?sso=webauth&cancel_login=1&again=yes')

usernamefield = driver.find_element_by_id("username")
passwordfield = driver.find_element_by_id("password")
loginbutton = driver.find_element_by_css_selector('.form-element.form-button')

usernamefield.send_keys(username)
passwordfield.send_keys(password)
loginbutton.click()

time.sleep(5)

userbutton = driver.find_elements_by_css_selector('.button')[0]
userbutton.click()

time.sleep(5)

driver.get('https://digicampus.uni-augsburg.de/dispatch.php/my_courses')

